package com.revolve44.mywindturbine

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.revolve44.mywindturbine.storage.AppPreferences
import com.revolve44.mywindturbine.ui.About
import com.revolve44.mywindturbine.ui.Calibration
import com.revolve44.mywindturbine.ui.dashboard.DashboardFragment
import com.revolve44.mywindturbine.ui.home.HomeFragment
import com.revolve44.mywindturbine.ui.setturbine.LocationActivity
import com.revolve44.mywindturbine.utils.AirBender
import com.revolve44.mywindturbine.utils.Api
import kotlinx.android.synthetic.main.fragment_home.*


/*
created in 2020
TODO: Attention! after every changes in code you must write in history directory what you change.
Current version 1.01 ALFA
 */

/*
History of app
from 2020/07/17

1.01
Crash app when he started - fixed in AirBender (add try/catch in DataMap)
HomeFragment do not showing data of forecast after first launch - he start showing ONLY in second try - fixed in HomeFragmnt

 */


class MainActivity : AppCompatActivity()  {
    val fragment1: Fragment = HomeFragment()
    val fragment2: Fragment = DashboardFragment()
    //val fragment3: Fragment = NotificationsFragment()

    val fm = supportFragmentManager
    var active = fragment1

    private val api: Api = Api() // now is correct may response
    private val homefrag: HomeFragment = HomeFragment() // now is correct may response
    private val airbender: AirBender = AirBender()

    private lateinit var mFirebaseAnalytics: FirebaseAnalytics

    //private var textactbar: TextView? = null
//    private var loaderanim: ImageView? = null
    private val handler = Handler()



    @RequiresApi(Build.VERSION_CODES.N)
    fun FirstLaunch() {
        // Obtain the FirebaseAnalytics instance.
        try {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        }catch (e: Exception){
            Log.d("Firebase ", "ERROR")
        }

        if (AppPreferences.firststart) {
            val intent =
                Intent(this, LocationActivity::class.java) // here bug been , relation with extra-
            startActivity(intent)
        }else{
            //-----------------------Startup App--------------------------------
            launchPad()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppPreferences.init(this) // TODO: this initializator may stay in start of Activity, when we do not have errors
        FirstLaunch()
        setContentView(R.layout.activity_main)

        val myToolbar =
            findViewById<View>(R.id.my_toolbar) as Toolbar
        setSupportActionBar(myToolbar)
        //loaderanim: = findViewById<ImageView>(R.id.refreshicon)

        //myToolbar.title = "My wind turbine"

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        val navigation =
            findViewById<View>(R.id.nav_view) as BottomNavigationView
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        //I added this if statement to keep the selected fragment when rotating the device
        //I added this if statement to keep the selected fragment when rotating the device
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(
                R.id.nav_host_fragment,
                HomeFragment()
            ).addToBackStack(null).commit()
        }

        //open and .hide fragment in one moment
        fm.beginTransaction().add(R.id.nav_host_fragment, fragment2, "2").hide( fragment2).commit()
        //fm.beginTransaction().add(R.id.nav_host_fragment, fragment3, "3").hide(fragment3).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment, fragment1, "1").commit()

//        if (!AppPreferences.firststart){
//
//        }
        //textactbar = findViewById<TextView>(R.id.toolbartext) as TextView

        Log.d(" lifecycle ", "onCreate  MainActivity")

    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onResume() {
        super.onResume()
        Log.d(" lifecycle ", "onResume  MainActivity")
        //LaunchPad()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onRestart() {
        super.onRestart()


        Log.d(" lifecycle ", "onRestart  MainActivity")
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun launchPad(){
        Log.d(" lifecycle ", "LauchPad  MainActivity")
        try {
            Snackbar.make(findViewById(android.R.id.content),"Loading ... ", 6000).show()
        }catch (e:Exception){
            Log.d(" snackbar ", "error in Activity")

        }


        api.startcall()
        Handler().postDelayed(
            {
                airbender.aang()
                Log.d("Threads num ", " is "+Thread.currentThread() + " UI is " + (Thread.currentThread() == Looper.getMainLooper().thread) )
                //Looper.myLooper() == Looper.getMainLooper()
                airbender.direction()
                Log.d("Threads num ", " is "+Thread.currentThread() + " UI is " + (Thread.currentThread() == Looper.getMainLooper().thread) )
                airbender.datamaploader()
                Log.d("Threads num ", " is "+Thread.currentThread() + " UI is " + (Thread.currentThread() == Looper.getMainLooper().thread) )
                airbender.TimeManipulation()

                Log.d("Threads num ", " is "+Thread.currentThread() + " UI is " + (Thread.currentThread() == Looper.getMainLooper().thread) )
                //homefrag.refreshData()
                try {
                    val params = Bundle()
                    params.putString("nominal_power", AppPreferences.nominalPower.toString())
                    mFirebaseAnalytics.logEvent("eventNominal", params)
                }catch (e:Exception){
                    Log.d("Firebase ", "ERROR")
                }


                Handler().postDelayed(
                    {
                        if (AppPreferences.direction!="--"){
                            homefrag.refreshData()
                        }
                    },100
                )
            },2000
        )

    }


    //switcher of fragmnets, he help for switching without loss filled form in fragments
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {

                    fm.beginTransaction().hide( active).show(fragment1).commit()
                    active = fragment1
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_dashboard -> {

                    fm.beginTransaction().hide(active).show(fragment2).commit()
                    active = fragment2
                    return@OnNavigationItemSelectedListener true
                }
//                R.id.navigation_notifications -> {
//
//                    fm.beginTransaction().hide(active).show(fragment3).commit()
//                    active = fragment3
//                    return@OnNavigationItemSelectedListener true
//                }

            }
            false
        }

    fun directionWind(view: View) {

        Snackbar.make(
            hommes, // Parent view
            "Wind Direction: "+AppPreferences.directionAngle+"°", // Message to show
            Snackbar.LENGTH_LONG // How long to display the message.
        ).show()

    }

    fun tonewact(view: View) {
        val intent = Intent(this, LocationActivity::class.java)
        startActivity(intent)
    }

    fun gotocalibr(view: View) {
        val intent = Intent(this, Calibration::class.java)
        startActivity(intent)

    }

    fun toabout(view: View) {
        val intent = Intent(this, About::class.java)
        startActivity(intent)
    }

    fun poweroutinfo(view: View) {
        Snackbar.make(
            hommes, // Parent view
            "Current forecasting power output", // Message to show
            Snackbar.LENGTH_LONG // How long to display the message.
        ).show()
    }
    fun cityinfo(view: View) {
        Snackbar.make(
            hommes, // Parent view
            "Current City ", // Message to show
            Snackbar.LENGTH_LONG // How long to display the message.
        ).show()
    }

    fun windspedonfo(view: View) {
        Snackbar.make(
            hommes, // Parent view
            "Wind Speed ", // Message to show
            Snackbar.LENGTH_LONG // How long to display the message.
        ).show()
    }


}
