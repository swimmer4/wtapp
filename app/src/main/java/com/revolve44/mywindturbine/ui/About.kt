package com.revolve44.mywindturbine.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.revolve44.mywindturbine.R

class About: AppCompatActivity() {
    private var step = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about_activity)


    }

    fun writetodev(view: View?) {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf("info@revolna.com"))
        i.putExtra(Intent.EXTRA_SUBJECT, "i have a question or suggestion")
        i.putExtra(Intent.EXTRA_TEXT, "So, ...")
        try {
            startActivity(Intent.createChooser(i, "Send mail..."))
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(this@About, "There are no email clients installed.", Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun gosite(view: View?) {
        goToUrl("http://revolna.com/")
    }

    fun goinstagram(view: View?) {
        goToUrl("https://www.instagram.com/revolna_workshop/")
    }

    fun gotelegram(view: View?) {
        goToUrl("https://t.me/solarpan")
    }

    private fun goToUrl(url: String) {
        val uriUrl = Uri.parse(url)
        val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(launchBrowser)
    }

    fun twitter(view: View?) {
        goToUrl("https://twitter.com/revolnaenergy")
    }

    fun firebirdMaster(view: View?) {
        step--
        if (step < 1) {
            goToUrl("https://en.wikipedia.org/wiki/James_Blyth_(engineer)")
        }
        Snackbar.make(
            findViewById<View>(android.R.id.content),
            step.toString() + " steps to the Easter Egg ",
            Snackbar.LENGTH_LONG
        ).show()
    }

    fun toGP(view: View) {
        goToUrl("https://play.google.com/store/apps/details?id=com.revolve44.fragments22")
    }

}