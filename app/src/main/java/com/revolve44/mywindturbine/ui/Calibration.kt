package com.revolve44.mywindturbine.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.revolve44.mywindturbine.MainActivity
import com.revolve44.mywindturbine.R
import com.revolve44.mywindturbine.storage.AppPreferences
import io.feeeei.circleseekbar.CircleSeekBar
import kotlinx.android.synthetic.main.calibration_activity.*
import kotlin.math.roundToInt

class Calibration: AppCompatActivity() {


    private lateinit var mSeekbar: CircleSeekBar
    private var curcalibrationTV: TextView? = null
    private var aftercalibrationpowerTV: TextView? = null
    private var calibrationdata: Float = 0f
    private var checker: Boolean = false
    private var curpow: Float = 0f
    private var coeff: Float = 1f
    private var chk = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.calibration_activity)

        mSeekbar = findViewById(R.id.seekbar)
        curcalibrationTV = findViewById(R.id.count)
        aftercalibrationpowerTV =findViewById(R.id.aftercalibrationpower)


        mSeekbar.maxProcess = 200
        mSeekbar.curProcess = AppPreferences.progressinSeekBar


        curpow = AppPreferences.currentPower
        coeff = AppPreferences.coefficient
        aftercalibrationpowerTV?.setText(""+(curpow* coeff).roundToInt())
        curcalibrationTV?.setText(""+mSeekbar.curProcess+" %")
        //Snackbar.make(calibrr, "Lets Start! ", Snackbar.LENGTH_LONG).show()




        //Listener i get it YEEEY
        mSeekbar.setOnSeekBarChangeListener { seekbar, curValue ->

            //calibrationdata = ((mSeekbar.curProcess /100).toFloat())
            coeff = ((mSeekbar.curProcess)/100f)
            curcalibrationTV?.setText(""+mSeekbar.curProcess+" %")


            // AppPreferences.currentPower = ((mSeekbar.curProcess)/100f)*curpow
            aftercalibrationpowerTV?.setText(""+(curpow* coeff).roundToInt())
            //aftercalibrationpowerTV?.setText(""+(((mSeekbar.curProcess)/100)*curpow))

            if ((AppPreferences.currentPower* coeff)> AppPreferences.nominalPower){
                Snackbar.make(calibrr, "Calibrated power cannot be greater than nominal power", Snackbar.LENGTH_LONG).show()

                mSeekbar.curProcess = 100
            }




            checker = true
            //clearSensordata(root)




            //val MSB =  Snackbar.make(activity!!.findViewById(android.R.id.content),"This is a simple Snackbar",Snackbar.LENGTH_LONG)
        }


    }

    override fun onPause() {
        super.onPause()

        coeff = ((mSeekbar.curProcess)/100f)
        AppPreferences.progressinSeekBar = mSeekbar.curProcess
        AppPreferences.coefficient = coeff
//        if ((AppPreferences.currentPower* AppPreferences.coefficient)< AppPreferences.nominalPower){
////            if (chk >0){
////                //view?.let { Snackbar.make(it, "Now swipe down in Home - to update data with new settings ", Snackbar.LENGTH_LONG).show() };
////
////            }
//        }
    }

    fun getback1(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}